using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a line.
    /// All points are expressed in absolute coordinates.
    /// </summary>
    public class LineSegment : LineBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildLine(PointF start)
        {
            return new[] { start, EndPoint };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"L {F(EndPoint.X)} {F(EndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF EndPoint { get; init; }
    }
}