using Spooksoft.Geometry.TwoDimensional;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Begins a new path in specified absolute coordinates
    /// </summary>
    public class MoveSegment : Segment
    {
        // Internal methods ---------------------------------------------------

        internal override Vector2D? GetPoint(PointF start, PointF control, float factor)
        {
            return null;
        }

        internal override (PointF endPoint, PointF lastControlPoint, bool newPathStarted) AddToLines(PointF start, PointF lastControlPoint, List<FixedVector2D> lines, int firstLineIndex)
        {
            return (EndPoint, EndPoint, true);
        }

        internal override (PointF endPoint, PointF lastControlPoint) AddToGeometry(PointF start, PointF lastControlPoint, GraphicsPath path)
        {
            if (path != null)
                path.StartFigure();

            return (EndPoint, EndPoint);
        }

        internal override (PointF endPoint, PointF lastControlPoint) AddToGeometry(PointF start, PointF lastControlPoint, GraphicsPath path, float? cutFrom, float? cutTo)
        {
            return AddToGeometry(start, lastControlPoint, path);
        }

        internal override string ToPathString() => $"M {F(EndPoint.X)} {F(EndPoint.Y)}";

        internal override (float length, PointF endPoint, PointF lastControlPoint) EvalLength(PointF start, PointF lastControlPoint)
        {
            return (0.0f, EndPoint, EndPoint);
        }

        // Public properties --------------------------------------------------

        public PointF EndPoint { get; init; }
    }
}