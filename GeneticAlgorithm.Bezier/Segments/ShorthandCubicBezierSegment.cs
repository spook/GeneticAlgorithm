using GeneticAlgorithm.Bezier.Tools;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a cubic Bezier curve.
    /// First point of the curve equals to the last point of previous
    /// path element. First control point is deduced from the previous
    /// path element as a mirror of its last control point against its
    /// endpoint.
    /// All points are expressed in absolute coordinates.
    /// </summary>
    public class ShorthandCubicBezierSegment : BaseCubicBezierBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildBezier(PointF start, PointF lastControlPoint)
        {
            var delta = start.Subtract(lastControlPoint);
            var controlPoint1 = start.Add(delta);

            return new PointF[] { start, controlPoint1, ControlPoint2, EndPoint };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"S {F(ControlPoint2.X)} {F(ControlPoint2.Y)} {F(EndPoint.X)} {F(EndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF ControlPoint2 { get; init; }
        public PointF EndPoint { get; init; }
    }
}