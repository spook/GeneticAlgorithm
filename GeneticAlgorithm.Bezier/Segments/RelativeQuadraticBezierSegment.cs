using GeneticAlgorithm.Bezier.Tools;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a quadratic Bezier curve.
    /// First point of the curve equals to the last point of previous
    /// path element. 
    /// All points are expressed in relative coordinates.
    /// </summary>
    public class RelativeQuadraticBezierSegment : BaseQuadraticBezierSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildBezier(PointF start, PointF lastControlPoint)
        {
            // Note: deltas are evaluated differently in case of this element
            // See: https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths#b%C3%A9zier_curves

            var controlPoint = start.Add(DeltaControlPoint);
            var end = start.Add(DeltaEndPoint);

            (var controlPoint1, var controlPoint2) = EstimateCubicControlPoints(start, controlPoint, end);
            return new[] { start, controlPoint1, controlPoint2, end };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"q {F(DeltaControlPoint.X)} {F(DeltaControlPoint.Y)} {F(DeltaEndPoint.X)} {F(DeltaEndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF DeltaControlPoint { get; init; }
        public PointF DeltaEndPoint { get; init; }
    }
}