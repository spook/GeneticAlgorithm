﻿using GeneticAlgorithm.Bezier.Tools;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Base class for all path elements.
    /// </summary>
    public abstract class Segment
    {
        // Private constants --------------------------------------------------

        private const double BezierSegmentLength = 5.0;

        // Protected types ----------------------------------------------------

        /// <summary>
        /// Utility class to simplify handling relative positions
        /// </summary>
        protected class RelativePoint
        {
            private PointF point;

            public RelativePoint(PointF start)
            {
                point = start;
            }

            public PointF Delta(PointF delta)
            {
                return new PointF(point.X + delta.X, point.Y + delta.Y);
            }

            public PointF Current => point;
        }

        // Protected methods --------------------------------------------------

        /// <summary>
        /// Formats a float into a SVG path-compatible string.
        /// </summary>
        protected string F(float value) => string.Format(CultureInfo.InvariantCulture, "{0:.##}", value);

        protected void AddBezierToLines(PointF[] bezier, List<FixedVector2D> lines)
        {
            (float length, _) = BezierCalculator.EstimateLength(bezier);

            var count = Math.Max(2, (int)(length / BezierSegmentLength + 1));

            var lastPoint = BezierCalculator.GetPointOn(bezier, 0.0f);
            for (int i = 1; i <= count; i++)
            {
                var t = i / (float)count;
                var nextPoint = BezierCalculator.GetPointOn(bezier, t);

                lines.Add(new FixedVector2D(lastPoint.X, lastPoint.Y, nextPoint.X, nextPoint.Y));

                lastPoint = nextPoint;
            }
        }

        // Internal methods -----------------------------------------------------

        internal abstract Vector2D? GetPoint(PointF start, PointF control, float factor);

        internal abstract (float length, PointF endPoint, PointF lastControlPoint) EvalLength(PointF start, PointF lastControlPoint);

        internal abstract (PointF endPoint, PointF lastControlPoint, bool newPathStarted) AddToLines(PointF start, PointF lastControlPoint, List<FixedVector2D> lines, int firstLineIndex);

        internal abstract (PointF endPoint, PointF lastControlPoint) AddToGeometry(PointF start, PointF lastControlPoint, GraphicsPath path);

        internal abstract (PointF endPoint, PointF lastControlPoint) AddToGeometry(PointF start, PointF lastControlPoint, GraphicsPath path, float? cutFrom, float? cutTo);

        internal abstract string ToPathString();
    }
}
