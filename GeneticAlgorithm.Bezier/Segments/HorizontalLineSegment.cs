using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a horizontal line.
    /// All points are expressed in absolute coordinates.
    /// </summary>
    public class HorizontalLineSegment : LineBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildLine(PointF start)
        {
            PointF end = new PointF(X, start.Y);

            return new[] { start, end };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"H {F(X)}";

        // Public properties --------------------------------------------------

        public float X { get; init; }
    }
}