using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a cubic Bezier curve.
    /// First point of the curve equals to the last point of previous
    /// path element.
    /// All points are expressed in relative coordinates.
    /// </summary>
    public class RelativeCubicBezierSegment : BaseCubicBezierBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildBezier(PointF start, PointF lastControlPoint)
        {
            RelativePoint point = new RelativePoint(start);

            PointF controlPoint1 = point.Delta(DeltaControlPoint1);
            PointF controlPoint2 = point.Delta(DeltaControlPoint2);
            PointF endPoint = point.Delta(DeltaEndPoint);

            return new[] { start, controlPoint1, controlPoint2, endPoint };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"c {F(DeltaControlPoint1.X)} {F(DeltaControlPoint1.Y)} {F(DeltaControlPoint2.X)} {F(DeltaControlPoint2.Y)} {F(DeltaEndPoint.X)} {F(DeltaEndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF DeltaControlPoint1 { get; init; }
        public PointF DeltaControlPoint2 { get; init; }
        public PointF DeltaEndPoint { get; init; }
    }
}