﻿using GeneticAlgorithm.Bezier.Utilities;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Draws a path on a scene.
    /// </summary>
    public class Path : Shape
    {
        // Private constants --------------------------------------------------

        private const float PointDistance = 10.0f;

        // Private methods ----------------------------------------------------

        private void PaintPath(BitmapBuffer buffer, GraphicsPath path)
        {
            if (Brush != null)
            {
                buffer.Graphics.FillPath(Brush, path);
            }

            if (Pen != null)
            {
                buffer.Graphics.DrawPath(Pen, path);
            }
        }

        // Protected methods --------------------------------------------------

        protected override void InternalRender(BitmapBuffer buffer)
        {
            RenderFullPath(buffer);
        }

        private void RenderFullPath(BitmapBuffer buffer)
        {
            GraphicsPath path = new();
            PointF start = new(0.0f, 0.0f);
            PointF lastControlPoint = new(0.0f, 0.0f);

            foreach (var pathElement in Definition)
                (start, lastControlPoint) = pathElement.AddToGeometry(start, lastControlPoint, path);

            PaintPath(buffer, path);
        }

        // Public methods -----------------------------------------------------

        public List<FixedVector2D> BuildLines()
        {
            List<FixedVector2D> lines = new();
            PointF start = new(0.0f, 0.0f);
            PointF lastControlPoint = new(0.0f, 0.0f);

            int pathStart = 0;
            for (int i = 0; i < Definition.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine($"Before {i}:\t{start} ; {lastControlPoint}");

                (start, lastControlPoint, bool newPath) = Definition[i].AddToLines(start, lastControlPoint, lines, pathStart);

                if (newPath)
                    pathStart = lines.Count;
            }

            return lines;
        }

        public List<Vector2D> BuildPoints()
        {
            // Evaluate lengths of elements and total length of the whole path
            PointF start = new(0.0f, 0.0f);
            PointF lastControlPoint = new(0.0f, 0.0f);

            List<float> lengths = new();
            for (int i = 0; i < Definition.Count; i++)
            {
                var pathElement = Definition[i];

                float length;
                (length, start, lastControlPoint) = pathElement.EvalLength(start, lastControlPoint);

                lengths.Add(length);
            }

            // Eval lengths

            var totalLength = lengths.Sum();
            var step = Math.Min(0.5f, PointDistance / totalLength);

            void evalElementAndFactor(float? absolutePosition, ref int element, ref float factor)
            {
                if (absolutePosition.HasValue)
                {
                    var positionLength = totalLength * absolutePosition.Value;

                    float lengthAcc = 0.0f;
                    int i = 0;
                    while (i < Definition.Count && lengthAcc + lengths[i] < positionLength)
                    {
                        lengthAcc += lengths[i];
                        i++;
                    }

                    // In this case there is nothing to draw (ie. from = 1)
                    if (i == Definition.Count)
                        return;

                    element = i;

                    // FromLength now should be smaller than lengths[i]
                    positionLength -= lengthAcc;
                    factor = lengths[i] > 0 ? positionLength / lengths[i] : 0.0f;
                }
            }

            // Dry run to figure out start points and control points
            List<(PointF start, PointF control)> controlPoints = new();
            start = new(0.0f, 0.0f);
            lastControlPoint = new(0.0f, 0.0f);
            controlPoints.Add((start, lastControlPoint));
            for (int i = 0; i < Definition.Count; i++)
            {
                (start, lastControlPoint) = Definition[i].AddToGeometry(start, lastControlPoint, null);
                controlPoints.Add((start, lastControlPoint));
            }

            // Now building points

            List<Vector2D> result = new();

            float factor = 0.0f;
            while (factor < 1.0f)
            {
                int element = -1;
                float elementFactor = float.NaN;

                evalElementAndFactor(factor, ref element, ref elementFactor);

                var point = Definition[element].GetPoint(controlPoints[element].start, controlPoints[element].control, elementFactor);
                if (point != null)
                    result.Add(point.Value);

                factor += step;
            }

            return result;
        }


        // Public properties --------------------------------------------------

        public IReadOnlyList<Segment> Definition { get; init; }
    }
}
