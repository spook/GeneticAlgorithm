﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Base class for all shapes drawn on the scene.
    /// </summary>
    public abstract class Shape : Visual
    {
        public Brush Brush { get; init; }

        public Pen Pen { get; init; }
    }
}
