using GeneticAlgorithm.Bezier.Tools;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a cubic Bezier curve.
    /// First point of the curve equals to the last point of previous
    /// path element. First control point is deduced from the previous
    /// path element as a mirror of its last control point against its
    /// endpoint.
    /// All points are expressed in relative coordinates.
    /// </summary>
    public class RelativeShorthandCubicBezierSegment : BaseCubicBezierBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildBezier(PointF start, PointF lastControlPoint)
        {
            var delta = start.Subtract(lastControlPoint);
            var controlPoint1 = start.Add(delta);

            RelativePoint point = new RelativePoint(controlPoint1);

            PointF controlPoint2 = point.Delta(DeltaControlPoint2);
            PointF endPoint = point.Delta(DeltaEndPoint);

            return new[] { start, controlPoint1, controlPoint2, endPoint };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"s {F(DeltaControlPoint2.X)} {F(DeltaControlPoint2.Y)} {F(DeltaEndPoint.X)} {F(DeltaEndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF DeltaControlPoint2 { get; init; }
        public PointF DeltaEndPoint { get; init; }
    }
}