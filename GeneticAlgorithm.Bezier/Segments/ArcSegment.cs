using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as an elliptical arc.
    /// First point of the arc equals to the last point of previous
    /// path element.
    /// Endpoint of the arc is expressed in absolute coordinates.
    /// </summary>
    public class ArcSegment : BaseArcSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[][] BuildBeziers(PointF start)
        {
            return InternalBuildBeziers(start, RX, RY, Angle, LargeArcFlag, SweepFlag, EndPoint);
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"A {F(RX)} {F(RY)} {F(Angle)} {(LargeArcFlag ? 1 : 0)} {(SweepFlag ? 1 : 0)} {F(EndPoint.X)} {F(EndPoint.Y)}";

        // Public properties --------------------------------------------------

        public float RX { get; init; }
        public float RY { get; init; }
        public float Angle { get; init; }
        public bool LargeArcFlag { get; init; }
        public bool SweepFlag { get; init; }
        public PointF EndPoint { get; init; }
    }
}