using GeneticAlgorithm.Bezier.Tools;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a line.
    /// All points are expressed in relative coordinates.
    /// </summary>
    public class RelativeLineSegment : LineBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildLine(PointF start)
        {
            var end = start.Add(DeltaEndPoint);

            return new[] { start, end };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"l {F(DeltaEndPoint.X)} {F(DeltaEndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF DeltaEndPoint { get; init; }
    }
}