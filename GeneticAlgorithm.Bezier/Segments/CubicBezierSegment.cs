using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a cubic Bezier curve.
    /// First point of the curve equals to the last point of previous
    /// path element.
    /// All points are expressed in absolute coordinates.
    /// </summary>
    public class CubicBezierSegment : BaseCubicBezierBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildBezier(PointF endPoint, PointF lastControlPoint)
        {
            return new PointF[] { endPoint, ControlPoint1, ControlPoint2, EndPoint };
        }

        // Public methods -----------------------------------------------------

        internal override string ToPathString() => $"C {F(ControlPoint1.X)} {F(ControlPoint1.Y)} {F(ControlPoint2.X)} {F(ControlPoint2.Y)} {F(EndPoint.X)} {F(EndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF ControlPoint1 { get; init; }

        public PointF ControlPoint2 { get; init; }

        public PointF EndPoint { get; init; }
    }
}