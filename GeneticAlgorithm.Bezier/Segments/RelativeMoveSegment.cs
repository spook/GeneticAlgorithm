using GeneticAlgorithm.Bezier.Tools;
using Spooksoft.Geometry.TwoDimensional;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Begins a new path in specified relative coordinates
    /// </summary>
    public class RelativeMoveSegment : Segment
    {
        // Internal methods ---------------------------------------------------

        internal override Vector2D? GetPoint(PointF start, PointF control, float factor)
        {
            return null;
        }

        internal override string ToPathString() => $"m {F(DeltaEndPoint.X)} {F(DeltaEndPoint.Y)}";

        internal override (PointF endPoint, PointF lastControlPoint, bool newPathStarted) AddToLines(PointF start, PointF lastControlPoint, List<FixedVector2D> lines, int firstLineIndex)
        {
            PointF endPoint = start.Add(DeltaEndPoint);

            return (endPoint, endPoint, true);
        }

        internal override (PointF endPoint, PointF lastControlPoint) AddToGeometry(PointF start, PointF lastControlPoint, GraphicsPath path)
        {
            if (path != null)
                path.StartFigure();

            PointF endPoint = start.Add(DeltaEndPoint);

            return (endPoint, endPoint);
        }

        internal override (PointF endPoint, PointF lastControlPoint) AddToGeometry(PointF start, PointF lastControlPoint, GraphicsPath path, float? cutFrom, float? cutTo)
        {
            return AddToGeometry(start, lastControlPoint, path);
        }

        internal override (float length, PointF endPoint, PointF lastControlPoint) EvalLength(PointF start, PointF lastControlPoint)
        {
            PointF endPoint = start.Add(DeltaEndPoint);

            return (0.0f, endPoint, endPoint);
        }

        // Public properties --------------------------------------------------

        public PointF DeltaEndPoint { get; init; }
    }
}