﻿using GeneticAlgorithm.Bezier.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents basic element, which can be drawn on the scene.
    /// </summary>
    public abstract class Visual
    {
        // Protected methods --------------------------------------------------

        protected abstract void InternalRender(BitmapBuffer buffer);

        protected Matrix BuildTransformMatrix()
        {
            var result = new Matrix();

            result.Translate(-Origin.X, -Origin.Y, MatrixOrder.Append);
            result.Translate(Position.X, Position.Y, MatrixOrder.Append);

            return result;
        }

        // Internal methods ---------------------------------------------------

        internal void Render(BitmapBuffer buffer)
        {
            var originalTransform = buffer.Graphics.Transform;

            var transform = originalTransform.Clone();
            transform.Multiply(BuildTransformMatrix(), MatrixOrder.Prepend);
            buffer.Graphics.Transform = transform;

            InternalRender(buffer);

            buffer.Graphics.Transform = originalTransform;
        }

        // Public properties --------------------------------------------------

        public PointF Position { get; init; }

        public PointF Origin { get; init; }
    }
}