using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a vertical line.
    /// All points are expressed in absolute coordinates.
    /// </summary>
    public class VerticalLineSegment : LineBasedSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildLine(PointF start)
        {
            PointF end = new PointF(start.X, Y);

            return new[] { start, end };
        }

        internal override string ToPathString() => $"V {F(Y)}";

        // Public properties --------------------------------------------------
        public float Y { get; init; }
    }
}