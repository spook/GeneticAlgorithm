using GeneticAlgorithm.Bezier.Tools;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GeneticAlgorithm.Bezier.Segments
{
    /// <summary>
    /// Represents a path part, which is drawn as a quadratic Bezier curve.
    /// Start point of the curve equals to the last point of previous
    /// path element. The control point is deduced from the previous
    /// path element as a mirror of its last control point against its
    /// endpoint.
    /// End point is expressed in relative coordinates.
    /// </summary>
    public class RelativeShorthandQuadraticBezierSegment : BaseQuadraticBezierSegment
    {
        // Protected methods --------------------------------------------------

        protected override PointF[] BuildBezier(PointF start, PointF lastControlPoint)
        {
            var delta = start.Subtract(lastControlPoint);
            var controlPoint = start.Add(delta);

            var end = start.Add(DeltaEndPoint);

            (var controlPoint1, var controlPoint2) = EstimateCubicControlPoints(start, controlPoint, end);

            return new[] { start, controlPoint1, controlPoint2, end };
        }

        // Internal methods ---------------------------------------------------

        internal override string ToPathString() => $"t {F(DeltaEndPoint.X)} {F(DeltaEndPoint.Y)}";

        // Public properties --------------------------------------------------

        public PointF DeltaEndPoint { get; init; }
    }
}