﻿using GeneticAlgorithm.Cars.Simple;
using GeneticAlgorithm.Cars.Drifting;
using GeneticAlgorithm.Engine.Infrastructure.Loading;
using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Models;
using GeneticAlgorithm.Engine.Types;
using GeneticAlgorithm.Engine.Utils;
using Spooksoft.Geometry.Extensions;
using System;
using System.Drawing;
using System.Xml.Serialization;
using GeneticAlgorithm.Cars.Space;

namespace GeneticAlgorithm
{
    public static partial class Program
    {
        // Private constants --------------------------------------------------

        private const int DRIVER_COUNT = 1000;
        private const int GENERATION_COUNT = 10000;
        private const int BEST_DRIVERS = 20;
        private const int DRIVERS_TO_STORE = 20;
        private const int RENDER_EVERY_NTH_GEN = 1;
        private const int REPEATED_BEST_RESULT_GENERATIONS = 10;
        private const int GENERATIONS_WITHOUT_IMPROVEMENT_RESET = 50;

        private const string TRACK_PATH = @"D:\Dokumenty\Dev\VS\GeneticAlgorithm\Tracks\Track1.xml";

        // Private methods ----------------------------------------------------

        private static void RandomizeDrivers(List<NeuralNetworkDecisionMaker> drivers, double range)
        {
            for (int i = 0; i < DRIVER_COUNT; i++)
                drivers.Add(new NeuralNetworkDecisionMaker(range, Color.Black));
        }

        private static void RenderGeneration(Track track, int gen, SimulationResult[] results)
        {
            Bitmap bitmap = Simulator.VisualizeResults(track, results);
            bitmap.Save($"D:\\generation{gen}.png");
        }

        private static ICarFactory BuildSimpleCarFactory()
        {
            // Simple car
            var carDefinition = new SimpleCarDefinition(new PixelsPerSecondSquared(10.0),
                new PixelsPerSecondSquared(-10.0),
                new PixelsPerSecond(60.0),
                new DegreesPerSecondSquared(10.0),
                new DegreesPerSecond(30.0));

            var carFactory = new SimpleCarFactory(carDefinition);
            return carFactory;            
        }

        private static ICarFactory BuildDriftingCarFactory()
        {
            // Simple car
            var carDefinition = new DriftingCarDefinition(new PixelsPerSecondSquared(10.0),
                new PixelsPerSecondSquared(-2.0),
                new PixelsPerSecond(-30.0),
                new PixelsPerSecond(60.0),
                new DegreesPerSecondSquared(10.0),
                new DegreesPerSecond(30.0),
                new PixelsPerSecondSquared(1.5));

            var carFactory = new DriftingCarFactory(carDefinition);
            return carFactory;
        }

        private static ICarFactory BuildSpaceCarFactory()
        {
            var carDefinition = new SpaceCarDefinition(new PixelsPerSecondSquared(10.0),
                new PixelsPerSecondSquared(-10.0),
                new PixelsPerSecond(60.0),
                new DegreesPerSecondSquared(10.0),
                new DegreesPerSecond(30.0));

            var carFactory = new SpaceCarFactory(carDefinition);
            return carFactory;
        }

        private static List<NeuralNetworkDecisionMaker> BuildInitialDrivers()
        {
            var drivers = new List<NeuralNetworkDecisionMaker>();

            var dummyDriver = new NeuralNetworkDecisionMaker(0.0, Color.Black);
            var name = dummyDriver.GetFileName();

            if (File.Exists($"D:\\{name}"))
            {
                Console.Write("Stored drivers detected. Do you want to use them as seed? (Y/N): ");
                var c = Console.ReadKey();

                if (c.Key == ConsoleKey.Y)
                {
                    using var fs = new FileStream($"D:\\{name}", FileMode.Open, FileAccess.Read);
                    using var reader = new BinaryReader(fs);

                    int count = reader.ReadInt32();
                    for (int i = 0; i < count; i++)
                        drivers.Add(new NeuralNetworkDecisionMaker(fs, Color.Black));
                }
                else
                {
                    RandomizeDrivers(drivers, 1.0);
                }
            }
            else
            {
                RandomizeDrivers(drivers, 1.0);
            }

            return drivers;
        }

        private static Track LoadTrack()
        {
            var loader = new TrackLoader();
            var track = loader.Load(TRACK_PATH);
            return track;
        }

        private static void CleanFiles()
        {
            foreach (var file in Directory.GetFiles("D:\\", "generation*.png"))
                File.Delete(file);
            foreach (var file in Directory.GetFiles("D:\\", "frame*.png"))
                File.Delete(file);
        }

        private static List<NeuralNetworkDecisionMaker> GenerateNewDriverGeneration(List<(NeuralNetworkDecisionMaker driver, SimulationResult result)> bestDrivers)
        {
            var drivers = new List<NeuralNetworkDecisionMaker>();

            for (int i = 0; i < bestDrivers.Count; i++)
                drivers.Add(bestDrivers[i].driver);

            // Crossing and mutating best drivers

            Random random = new Random();

            var listAddSync = new object();

            Func<(double weight, double bias), (double weight, double bias)> GenerateMutator(double mutationRange)
            {
                return ((double weight, double bias) factor) =>
                {
                    var newWeight = (factor.weight + random.NextDouble() * (2.0 * mutationRange) - mutationRange);//.Clamp(-10.0, 10.0);
                    var newBias = (factor.bias + random.NextDouble() * (2.0 * mutationRange) - mutationRange);//.Clamp(-10.0, 10.0);

                    return (newWeight, newBias);
                };
            }

            for (int i = 0; i < Math.Min(bestDrivers.Count, 3); i++)
            {
                for (int j = 0; j < 10; j++) 
                {
                    var mutatedDriver = new NeuralNetworkDecisionMaker(bestDrivers[i].driver,
                            bestDrivers[0].driver,
                            0.0,
                            0.1,
                            GenerateMutator(0.2),
                            Color.Violet);

                    drivers.Add(mutatedDriver);

                    mutatedDriver = new NeuralNetworkDecisionMaker(bestDrivers[i].driver,
                            bestDrivers[0].driver,
                            0.0,
                            0.2,
                            GenerateMutator(0.05),
                            Color.DarkViolet);

                    drivers.Add(mutatedDriver);
                }
            }

            Parallel.For(0, bestDrivers.Count, i =>
            {
                for (int j = i; j < bestDrivers.Count; j++)
                {
                    // Minor adjustments
                    var mutatedDriver = new NeuralNetworkDecisionMaker(bestDrivers[i].driver,
                        bestDrivers[j].driver,
                        0.3,
                        0.3,
                        GenerateMutator(1.0),
                        Color.Green);

                    lock (listAddSync)
                    {
                        drivers.Add(mutatedDriver);
                    }

                    // Major crossover
                    mutatedDriver = new NeuralNetworkDecisionMaker(bestDrivers[i].driver,
                        bestDrivers[j].driver,
                        0.5,
                        0.1,
                        GenerateMutator(0.2),
                        Color.Orange);

                    lock (listAddSync)
                    {
                        drivers.Add(mutatedDriver);
                    }

                    // Major mutations
                    mutatedDriver = new NeuralNetworkDecisionMaker(bestDrivers[i].driver,
                        bestDrivers[j].driver,
                        0.5,
                        0.5,
                        GenerateMutator(2.0),
                        Color.SkyBlue);

                    lock (listAddSync)
                    {
                        drivers.Add(mutatedDriver);
                    }
                }
            });
            return drivers;
        }

        private static void SaveBestDrivers(List<NeuralNetworkDecisionMaker> totalBestDrivers)
        {
            var name = totalBestDrivers[0].GetFileName();

            using (FileStream fs = new FileStream(@$"D:\{name}", FileMode.Create, FileAccess.Write))
            {
                using var writer = new BinaryWriter(fs);
                writer.Write(totalBestDrivers.Count);

                for (int i = 0; i < totalBestDrivers.Count; i++)
                {
                    var driver = totalBestDrivers[i];
                    driver.Save(fs);
                }
            }
        }

        private static List<(NeuralNetworkDecisionMaker driver, SimulationResult result)> GetBestDrivers(List<NeuralNetworkDecisionMaker> drivers, SimulationResult[] results)
        {
            return drivers.Zip<NeuralNetworkDecisionMaker, SimulationResult, (NeuralNetworkDecisionMaker driver, SimulationResult result)>(results, (d, r) => (d, r))
                                .OrderByDescending(x => x.result.FurthestPoint)
                                .ThenBy(x => x.result.SimulationTime.Value)
                                .Take(BEST_DRIVERS)
                                .ToList();
        }

        // Public methods -----------------------------------------------------

        public static void Main(string[] args)
        {
            CleanFiles();

            Track track = LoadTrack();
            List<NeuralNetworkDecisionMaker> drivers = BuildInitialDrivers();
            ICarFactory carFactory = BuildSimpleCarFactory();

            List<NeuralNetworkDecisionMaker> totalBestDrivers = new();

            var simulationParams = new SimulationParams(false,
                new Seconds(0.1),
                new Seconds(300));

            SimulationResult bestPartialResultSoFar = null;
            int bestPartialResultGeneration = -1;

            SimulationResult bestResultSoFar = null;
            int bestResultGeneration = -1;

            for (int gen = 0; gen < GENERATION_COUNT; gen++)
            {
                Console.WriteLine($"Generation {gen}");

                SimulationResult[] results = new SimulationResult[drivers.Count];

                // Run the simulation

                /*
                for (int i = 0; i < drivers.Count; i++)
                {
                    results[i] = Simulator.Simulate(track, carFactory, simulationParams, drivers[i]);
                }
                */
                                
                Parallel.For(0, drivers.Count, (i) =>
                {
                    results[i] = Simulator.Simulate(track, carFactory, simulationParams, drivers[i]);
                });
                

                var bestDrivers = GetBestDrivers(drivers, results);

                if (bestPartialResultSoFar == null || bestPartialResultSoFar.FurthestPoint < bestDrivers[0].result.FurthestPoint)
                {
                    bestPartialResultSoFar = bestDrivers[0].result;
                    bestPartialResultGeneration = gen;
                }

                if (bestResultGeneration == -1 && bestPartialResultGeneration != -1)
                {
                    int generationsSinceLastImprovement = gen - bestPartialResultGeneration;

                    if (generationsSinceLastImprovement > (GENERATIONS_WITHOUT_IMPROVEMENT_RESET - 10))
                    {
                        Console.WriteLine($"  Resetting drivers after {GENERATIONS_WITHOUT_IMPROVEMENT_RESET - generationsSinceLastImprovement} generations");
                    }
                    if (generationsSinceLastImprovement >= GENERATIONS_WITHOUT_IMPROVEMENT_RESET)
                    {
                        // Re-randomize drivers

                        drivers = BuildInitialDrivers();

                        bestPartialResultSoFar = null;
                        bestPartialResultGeneration = -1;

                        bestResultSoFar = null;
                        bestResultGeneration = -1;

                        continue;
                    }
                }

                if (bestDrivers[0].result.Finished)
                {
                    if (bestResultSoFar == null || bestResultSoFar.DriveTime.Value > bestDrivers[0].result.DriveTime.Value)
                    {
                        bestResultSoFar = bestDrivers[0].result;
                        bestResultGeneration = gen;
                    }
                    else if ((bestResultSoFar.DriveTime.Value - bestDrivers[0].result.DriveTime.Value).IsZero())
                    {
                        Console.WriteLine($"  Best result repeated for {gen - bestResultGeneration} generation(s)");

                        // Generations without better result
                        if (gen - bestResultGeneration > REPEATED_BEST_RESULT_GENERATIONS)
                        {
                            totalBestDrivers = bestDrivers
                                .Where(x => x.result.Finished)
                                .Take(DRIVERS_TO_STORE)
                                .Select(x => x.driver)
                                .ToList();

                            RenderGeneration(track, gen, results);
                            break;

                        }
                    }
                }

                if (gen % RENDER_EVERY_NTH_GEN == 0)
                    RenderGeneration(track, gen, results);

                Console.WriteLine($"  Best driver reached point: {bestDrivers[0].result.FurthestPoint}");
                Console.WriteLine($"  Best driver time: {bestDrivers[0].result.SimulationTime.Value} sec.");

                drivers = GenerateNewDriverGeneration(bestDrivers);
            }

            if (totalBestDrivers != null)
            {
                var debugSimulationParams = simulationParams with
                {
                    DebugOutput = true
                };

                Simulator.Simulate(track, carFactory, debugSimulationParams, totalBestDrivers[0]);
            }

            SaveBestDrivers(totalBestDrivers);
        }
    }
}