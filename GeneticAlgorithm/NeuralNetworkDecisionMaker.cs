﻿using GeneticAlgorithm.Engine.Infrastructure.NeuralNetwork;
using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Models;
using GeneticAlgorithm.Engine.Types;
using System.Drawing;
using System.Text;

namespace GeneticAlgorithm
{
    public static partial class Program
    {
        private const int INPUT_SIZE = 11;

        public class NeuralNetworkDecisionMaker : IDecisionMaker
        {
            private readonly Network network;
            private readonly List<double> inputCache = new(new double[INPUT_SIZE]);

            public NeuralNetworkDecisionMaker(NeuralNetworkDecisionMaker first, 
                NeuralNetworkDecisionMaker second, 
                double crossoverRatio, 
                double mutationProbability,
                Func<(double wieght, double bias), (double weight, double bias)> mutator,
                Color color)
            {
                Random random = new Random();

                if (first.network.Layers.Count != second.network.Layers.Count)
                    throw new ArgumentException(nameof(first));
                if (first.network.InputSize != second.network.InputSize)
                    throw new ArgumentException(nameof(first));

                for (int i = 0; i < first.network.Layers.Count; i++)
                {
                    if (first.network.Layers[i].Count != second.network.Layers[i].Count)
                        throw new ArgumentException(nameof(first));
                }

                this.network = first.network.Clone();

                for (int i = 0; i < first.network.Layers.Count; i++)
                {
                    for (int j = 0; j < first.network.Layers[i].Count; j++)
                    {
                        for (int k = 0; k < first.network.Layers[i].Neurons[j].Factors.Count; k++)
                        {
                            var factor = network.Layers[i].Neurons[j].Factors[k];

                            if (random.NextDouble() <= crossoverRatio)
                            {
                                // Take second
                                factor = second.network.Layers[i].Neurons[j].Factors[k];
                            }

                            if (random.NextDouble() <= mutationProbability)
                            {
                                factor = mutator(factor);
                            }

                            network.Layers[i].Neurons[j].Replace(k, factor);
                        }
                    }
                }
                
                this.Color = color;
            }

            public NeuralNetworkDecisionMaker(double randomRange, Color color)
            {
                network = new NetworkBuilder()
                    .AddInputLayer(INPUT_SIZE)
                    .AddLayer(20, ActivationFunction.Sigmoid, randomRange)
                    .AddLayer(20, ActivationFunction.Sigmoid, randomRange)
                    .AddLayer(2, ActivationFunction.Tanh, randomRange)
                    .Build();

                Color = color;
            }

            public NeuralNetworkDecisionMaker(Stream stream, Color color)
                : this(0.0, color)
            {
                var reader = new BinaryReader(stream);

                for (int l = 0; l < network.Layers.Count; l++)
                {
                    var layer = network.Layers[l];

                    for (int n = 0; n < layer.Neurons.Count; n++)
                    {
                        var neuron = layer.Neurons[n];

                        for (int f = 0; f < neuron.Factors.Count; f++)
                        {
                            var weight = reader.ReadDouble();
                            var bias = reader.ReadDouble();

                            neuron.Replace(f, (weight, bias));
                        }
                    }
                }
            }

            public string GetFileName()
            {
                StringBuilder result = new ();
                result.Append($"drivers-{network.InputSize}");

                for (int i = 0; i < network.Layers.Count; i++)
                    result.Append($"-{network.Layers[i].Count}");

                result.Append(".dat");

                return result.ToString();
            }

            public Decision Decide(double speedParam,
                double rotationParam,
                double normalizedCourseDiff,
                double distanceMinus60,
                double distanceMinus45,
                double distanceMinus30,
                double distanceMinus15,
                double distance0,
                double distancePlus15,
                double distancePlus30,
                double distancePlus45,
                double distancePlus60)
            {
                inputCache[0] = speedParam;
                inputCache[1] = rotationParam;
                inputCache[2] = distanceMinus60;
                inputCache[3] = distanceMinus45;
                inputCache[4] = distanceMinus30;
                inputCache[5] = distanceMinus15;
                inputCache[6] = distance0;
                inputCache[7] = distancePlus15;
                inputCache[8] = distancePlus30;
                inputCache[9] = distancePlus45;
                inputCache[10] = distancePlus60;
                // inputCache[2] = normalizedCourseDiff;

                var result = network.Evaluate(inputCache);

                return new Decision(result[0], result[1]);
            }

            public void Save(Stream stream)
            {
                var writer = new BinaryWriter(stream);

                for (int l = 0; l < network.Layers.Count; l++)
                {
                    var layer = network.Layers[l];

                    for (int n = 0; n < layer.Neurons.Count; n++)
                    {
                        var neuron = layer.Neurons[n];

                        for (int f = 0; f < neuron.Factors.Count; f++)
                        {
                            writer.Write(neuron.Factors[f].weight);
                            writer.Write(neuron.Factors[f].bias);
                        }
                    }
                }
            }

            public Color Color { get; }
        }
    }
}