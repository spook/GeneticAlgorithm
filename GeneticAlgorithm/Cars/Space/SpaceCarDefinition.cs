﻿using GeneticAlgorithm.Engine.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Space
{
    public record class SpaceCarDefinition
    {
        public SpaceCarDefinition(PixelsPerSecondSquared maxAcceleration,
            PixelsPerSecondSquared maxDeceleration,
            PixelsPerSecond maxSpeed,
            DegreesPerSecondSquared maxTurningRateChange,
            DegreesPerSecond maxRotationRate)
        {
            if (maxAcceleration.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxAcceleration));
            if (maxDeceleration.Value >= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxDeceleration));
            if (maxTurningRateChange.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxTurningRateChange));
            if (maxSpeed.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxSpeed));
            if (maxRotationRate.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxRotationRate));

            MaxAcceleration = maxAcceleration;
            MaxDeceleration = maxDeceleration;
            MaxSpeed = maxSpeed;
            MaxTurningRateChange = maxTurningRateChange;
            MaxRotationRate = maxRotationRate;
        }

        public PixelsPerSecondSquared MaxAcceleration { get; init; }
        public PixelsPerSecondSquared MaxDeceleration { get; init; }
        public PixelsPerSecond MaxSpeed { get; }
        public DegreesPerSecondSquared MaxTurningRateChange { get; init; }
        public DegreesPerSecond MaxRotationRate { get; }
    }
}
