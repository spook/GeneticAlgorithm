﻿using GeneticAlgorithm.Cars.Space;
using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Drifting
{
    public class SpaceCarFactory : ICarFactory
    {
        private readonly SpaceCarDefinition carDefinition;

        public SpaceCarFactory(SpaceCarDefinition carDefinition)
        {
            this.carDefinition = carDefinition;
        }

        public ICar BuildCar(Vector2D start, Degrees startOrientation) =>
            new SpaceCar(start, startOrientation, carDefinition);
    }
}
