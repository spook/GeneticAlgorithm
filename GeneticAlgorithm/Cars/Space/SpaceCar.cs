﻿using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Space
{
    public class SpaceCar : ICar
    {
        public static readonly Vector2D ZeroRotation = new Vector2D(0.0, 1.0);

        private readonly SpaceCarDefinition carDefinition;
        private Degrees direction;
        private DegreesPerSecond rotationRate;
        private Vector2D momentum;
        private PixelsPerSecondSquared thrust;

        public SpaceCar(Vector2D position, Degrees direction, SpaceCarDefinition carDefinition)
        {
            this.carDefinition = carDefinition;

            Position = position;
            this.direction = direction;
            this.rotationRate = DegreesPerSecond.Zero;
            this.thrust = PixelsPerSecondSquared.Zero;
            this.momentum = new Vector2D(0.0, 0.0);
        }

        public void ApplyMotionChange(double accelerationDecision, double rotationDecision, Seconds time)
        {
            var acceleration = accelerationDecision > 0 ?
                carDefinition.MaxAcceleration * accelerationDecision :
                carDefinition.MaxDeceleration * -accelerationDecision;
            var rotationRateChange = carDefinition.MaxTurningRateChange * rotationDecision;

            thrust = acceleration
                .Clamp(carDefinition.MaxDeceleration, carDefinition.MaxAcceleration);
            rotationRateChange = rotationRateChange
                .Clamp(-carDefinition.MaxTurningRateChange, carDefinition.MaxTurningRateChange);

            rotationRate += rotationRateChange * time;
        }

        public bool Move(Seconds time)
        {
            direction += rotationRate * time;

            var thrustVector = ZeroRotation
                .Rotate(direction.AsRadians().Value)
                .WithLength((thrust * time).Value);

            momentum += thrustVector;
            Position += momentum;

            // Space car is never slipping
            return false;
        }

        public Vector2D Position { get; private set; }
        public Degrees Direction => direction;

        public double NormalizedSpeed => momentum.Length / carDefinition.MaxSpeed.Value;
        public double NormalizedRotationRate => rotationRate / carDefinition.MaxRotationRate;

        public Vector2D Orientation => ZeroRotation.Rotate(direction.AsRadians().Value);
        public Vector2D Momentum => Orientation;
    }
}
