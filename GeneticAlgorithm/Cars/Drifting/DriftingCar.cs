﻿using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Drifting
{
    public class DriftingCar : ICar
    {
        public static readonly Vector2D ZeroRotation = new Vector2D(0.0, 1.0);

        private readonly DriftingCarDefinition carDefinition;
        private Degrees direction;
        private PixelsPerSecond speed;
        private DegreesPerSecond rotationRate;
        private Vector2D momentum;

        public DriftingCar(Vector2D position, Degrees direction, DriftingCarDefinition carDefinition)
        {
            this.carDefinition = carDefinition;

            Position = position;
            this.direction = direction;

            speed = carDefinition.MinSpeed.Value < 0 ? PixelsPerSecond.Zero : carDefinition.MinSpeed;
            rotationRate = DegreesPerSecond.Zero;
            momentum = new Vector2D(0.0, 0.0);
        }

        public void ApplyMotionChange(double accelerationDecision, double rotationDecision, Seconds time)
        {
            var acceleration = accelerationDecision > 0 ?
                carDefinition.MaxAcceleration * accelerationDecision :
                carDefinition.MaxDeceleration * -accelerationDecision;
            var rotationRateChange = carDefinition.MaxTurningRateChange * rotationDecision;

            acceleration = acceleration
                .Clamp(carDefinition.MaxDeceleration, carDefinition.MaxAcceleration);
            rotationRateChange = rotationRateChange
                .Clamp(-carDefinition.MaxTurningRateChange, carDefinition.MaxTurningRateChange);

            speed = (speed + acceleration * time)
                .Clamp(carDefinition.MinSpeed, carDefinition.MaxSpeed);
            rotationRate += (rotationRateChange * time)
                .Clamp(-carDefinition.MaxRotationRate, carDefinition.MaxRotationRate);
        }

        public bool Move(Seconds time)
        {
            direction += rotationRate * time;

            var orientation = ZeroRotation
                .Rotate(direction.AsRadians().Value)
                .WithLength((speed * time).Value);

            var possibleChange = orientation - momentum;
            bool slipping = false;
            if (possibleChange.Length > (carDefinition.MaxOrientationChange.Value * time.Value))
            {
                slipping = true;
                possibleChange = possibleChange.WithLength(carDefinition.MaxOrientationChange.Value * time.Value);
            }

            momentum += possibleChange;

            Position += momentum;

            return slipping;
        }

        public Vector2D Position { get; private set; }
        public Degrees Direction => direction;

        public double NormalizedSpeed => speed / carDefinition.MaxSpeed;
        public double NormalizedRotationRate => rotationRate / carDefinition.MaxRotationRate;

        public Vector2D Orientation => ZeroRotation.Rotate(direction.AsRadians().Value).WithLength(speed.Value);
        public Vector2D Momentum => momentum;
    }
}
