﻿using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Drifting
{
    public class DriftingCarFactory : ICarFactory
    {
        private readonly DriftingCarDefinition carDefinition;

        public DriftingCarFactory(DriftingCarDefinition carDefinition)
        {
            this.carDefinition = carDefinition;
        }

        public ICar BuildCar(Vector2D start, Degrees startOrientation) =>
            new DriftingCar(start, startOrientation, carDefinition);
    }
}
