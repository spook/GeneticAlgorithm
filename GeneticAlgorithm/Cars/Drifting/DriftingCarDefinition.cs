﻿using GeneticAlgorithm.Engine.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Drifting
{
    public record class DriftingCarDefinition
    {
        public DriftingCarDefinition(PixelsPerSecondSquared maxAcceleration,
            PixelsPerSecondSquared maxDeceleration,
            PixelsPerSecond minSpeed,
            PixelsPerSecond maxSpeed,
            DegreesPerSecondSquared maxTurningRateChange,
            DegreesPerSecond maxRotationRate,
            PixelsPerSecondSquared maxOrientationChange
            )
        {
            if (maxAcceleration.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxAcceleration));
            if (maxDeceleration.Value >= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxDeceleration));
            if (maxSpeed.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxSpeed));
            if (minSpeed.Value > maxSpeed.Value)
                throw new ArgumentOutOfRangeException(nameof(minSpeed));
            if (maxTurningRateChange.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxTurningRateChange));
            if (maxRotationRate.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxRotationRate));
            if (maxOrientationChange.Value <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(maxOrientationChange));

            MaxAcceleration = maxAcceleration;
            MaxDeceleration = maxDeceleration;
            MinSpeed = minSpeed;
            MaxSpeed = maxSpeed;
            MaxTurningRateChange = maxTurningRateChange;
            MaxRotationRate = maxRotationRate;
            MaxOrientationChange = maxOrientationChange;
        }

        public PixelsPerSecondSquared MaxAcceleration { get; init; }
        public PixelsPerSecondSquared MaxDeceleration { get; init; }
        public PixelsPerSecond MinSpeed { get; init; }
        public PixelsPerSecond MaxSpeed { get; init; }
        public DegreesPerSecondSquared MaxTurningRateChange { get; init; }
        public DegreesPerSecond MaxRotationRate { get; init; }
        public PixelsPerSecondSquared MaxOrientationChange { get; }
    }
}
