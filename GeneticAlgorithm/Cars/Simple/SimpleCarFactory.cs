﻿using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Simple
{
    public class SimpleCarFactory : ICarFactory
    {
        private readonly SimpleCarDefinition carDefinition;

        public SimpleCarFactory(SimpleCarDefinition carDefinition)
        {
            this.carDefinition = carDefinition;
        }

        public ICar BuildCar(Vector2D start, Degrees startOrientation) =>
            new SimpleCar(start, startOrientation, carDefinition);
    }
}
