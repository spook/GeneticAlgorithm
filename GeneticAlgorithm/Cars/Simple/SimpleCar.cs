﻿using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Cars.Simple
{
    public class SimpleCar : ICar
    {
        public static readonly Vector2D ZeroRotation = new Vector2D(0.0, 1.0);

        private readonly SimpleCarDefinition carDefinition;
        private Degrees direction;
        private PixelsPerSecond speed;
        private DegreesPerSecond rotationRate;

        public SimpleCar(Vector2D position, Degrees direction, SimpleCarDefinition carDefinition)
        {
            this.carDefinition = carDefinition;

            Position = position;
            this.direction = direction;

            speed = PixelsPerSecond.Zero;
            rotationRate = DegreesPerSecond.Zero;
        }

        public void ApplyMotionChange(double accelerationDecision, double rotationDecision, Seconds time)
        {
            var acceleration = accelerationDecision > 0 ?
                carDefinition.MaxAcceleration * accelerationDecision :
                carDefinition.MaxDeceleration * -accelerationDecision;
            var rotationRateChange = carDefinition.MaxTurningRateChange * rotationDecision;

            acceleration = acceleration
                .Clamp(carDefinition.MaxDeceleration, carDefinition.MaxAcceleration);
            rotationRateChange = rotationRateChange
                .Clamp(-carDefinition.MaxTurningRateChange, carDefinition.MaxTurningRateChange);

            speed = (speed + acceleration * time)
                .Clamp(-carDefinition.MaxSpeed, carDefinition.MaxSpeed);
            rotationRate += (rotationRateChange * time)
                .Clamp(-carDefinition.MaxRotationRate, carDefinition.MaxRotationRate);
        }

        public bool Move(Seconds time)
        {
            direction += rotationRate * time;

            var directionalVector = ZeroRotation
                .Rotate(direction.AsRadians().Value)
                .WithLength((speed * time).Value);

            Position += directionalVector;

            // Simple car is never slipping
            return false;
        }

        public Vector2D Position { get; private set; }
        public Degrees Direction => direction;

        public double NormalizedSpeed => speed / carDefinition.MaxSpeed;
        public double NormalizedRotationRate => rotationRate / carDefinition.MaxRotationRate;

        public Vector2D Orientation => ZeroRotation.Rotate(direction.AsRadians().Value).WithLength(speed.Value);
        public Vector2D Momentum => Orientation;
    }
}
