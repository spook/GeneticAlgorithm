﻿using GeneticAlgorithm.Bezier;
using GeneticAlgorithm.Bezier.Segments;
using GeneticAlgorithm.Engine.Infrastructure.Simulation;
using GeneticAlgorithm.Engine.Models;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GeneticAlgorithm.Engine.Infrastructure.Loading
{
    public class TrackLoader
    {
        public Track Load(string path)
        {
            using var fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            var serializer = new XmlSerializer(typeof(TrackModel));
            var track = (TrackModel)serializer.Deserialize(fs);

            // Processing area

            var areaSpan = new Vector2D(track.Area.Width, track.Area.Height);

            // Processing start

            var startPoint = new Vector2D(track.Start.X, track.Start.Y);
            var startOrientation = new Degrees(track.Start.Orientation);

            // Processing finish line

            var finishLine = new FixedVector2D(track.Finish.X1, track.Finish.Y1, track.Finish.X2, track.Finish.Y2);

            // Processing obstacles

            var pathSerializer = new PathElementsSerializer();
            List<Segment> segments = pathSerializer.Deserialize(track.Obstacles);
            var obstaclesPath = new GeneticAlgorithm.Bezier.Segments.Path { Definition = segments };
            List<FixedVector2D> obstacles = obstaclesPath.BuildLines();

            // Processing guide

            List<Segment> guideSegments = pathSerializer.Deserialize(track.Guide);
            var guidePath = new GeneticAlgorithm.Bezier.Segments.Path { Definition = guideSegments };
            List<Vector2D> guide = guidePath.BuildPoints();

            int[,] guideArray;

            string guideFile = System.IO.Path.ChangeExtension(path, ".guide");
            if (File.Exists(guideFile))
            {
                guideArray = ReadGuideArray(areaSpan, guideFile);
            }
            else
            {
                guideArray = BuildGuideArray(areaSpan, guide, track.MaxRoadWidth);
                WriteGuideArray(guideArray, areaSpan, guideFile);
            }

            // Extracting 

            return new Track(areaSpan, obstacles, guide, guideArray, finishLine, startPoint, startOrientation, track.MaxRoadWidth);
        }

        private void WriteGuideArray(int[,] guideArray, Vector2D areaSpan, string guideFile)
        {
            using var fs = new FileStream(guideFile, FileMode.Create, FileAccess.Write);
            using var bw = new BinaryWriter(fs);

            for (int x = 0; x < (int)areaSpan.X; x++)
                for (int y = 0; y < (int)areaSpan.Y; y++)
                    bw.Write(guideArray[x, y]);
        }

        private int[,] ReadGuideArray(Vector2D areaSpan, string guideFile)
        {
            var result = new int[(int)areaSpan.X, (int)areaSpan.Y];

            using var fs = new FileStream(guideFile, FileMode.Open, FileAccess.Read);
            using var br = new BinaryReader(fs);

            for (int x = 0; x < (int)areaSpan.X; x++)
                for (int y = 0; y < (int)areaSpan.Y; y++)
                    result[x, y] = br.ReadInt32();

            return result;
        }

        private int[,] BuildGuideArray(Vector2D areaSpan, List<Vector2D> guide, double maxRoadWidth)
        {
            Console.WriteLine("Building guide array...");

            var result = new int[(int)areaSpan.X, (int)areaSpan.Y];
            for (int x = 0; x < result.GetLength(0); x++)
                for (int y = 0; y < result.GetLength(1); y++)
                {
                    result[x, y] = -1;
                }
            
            for (int i = 0; i < guide.Count; i++)
            {
                Console.Write($"{i * 100 / guide.Count:0.#}%...");

                var x = (int)guide[i].X;
                var y = (int)guide[i].Y;

                var xMin = Math.Max(0, x - 2 * (int)maxRoadWidth);
                var xMax = Math.Min((int)areaSpan.X, x + 2 * (int)maxRoadWidth);
                var yMin = Math.Max(0, y - 2 * (int)maxRoadWidth);
                var yMax = Math.Min((int)areaSpan.Y, y + 2 * (int)maxRoadWidth);

                for (int x1 = xMin; x1 < xMax; x1++)
                    for (int y1 = yMin; y1 < yMax; y1++)
                    {
                        double dist = new FixedVector2D(x, y, x1, y1).Length;
                        int current = result[x1, y1];
                        double currentDist = current == -1 ? double.PositiveInfinity : new FixedVector2D(guide[current].X, guide[current].Y, x1, y1).Length;

                        if (dist < currentDist)
                            result[x1, y1] = i;
                    }
            }

            Console.WriteLine("Done.");

            return result;
        }
    }
}
