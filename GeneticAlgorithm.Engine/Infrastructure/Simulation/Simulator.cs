﻿using GeneticAlgorithm.Engine.Models;
using GeneticAlgorithm.Engine.Types;
using GeneticAlgorithm.Engine.Utils;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.Simulation
{
    public class Simulator
    {
        private static readonly Vector2D zeroRotation = new Vector2D(0.0, 1.0);

        private static SolidBrush carBrush;
        private static Pen carPen;
        private static Pen distLinePen;
        private static SolidBrush distLineTextBrush;
        private static SolidBrush whiteBrush;

        static Simulator()
        {
            carBrush = new SolidBrush(Color.FromArgb(255, 0, 32, 255));
            carPen = new Pen(Color.FromArgb(255, 0, 32, 255), 3.0f);
            distLinePen = new Pen(Color.FromArgb(128, 255, 0, 0), 2.0f);
            distLineTextBrush = new SolidBrush(Color.FromArgb(255, 128, 0, 0));
            whiteBrush = new SolidBrush(Color.FromArgb(255, 255, 255, 255));
        }

        private static void DrawOutlinedText(Graphics g, string text, Font font, Brush targetBrush, float x, float y)
        {
            for (int dx = -1; dx <= 1; dx++)
                for (int dy = -1; dy <= 1; dy++)
                    g.DrawString(text, font, whiteBrush, x + dx, y + dy);

            g.DrawString(text, font, targetBrush, x, y);
        }

        private static void DebugOutputFrame(Track track,
            ICar car,
            Pixels seekingRange,
            List<(double distance, Vector2D? point)> distances,
            int frame,
            List<(Vector2D, bool)> skidMarks)
        {
            Bitmap output = track.Render();
            using var g = Graphics.FromImage(output);

            GraphicsPath gp = new GraphicsPath();
            bool newPath = true;

            var skidPoints = new List<PointF>();

            for (int i = 0; i < skidMarks.Count; i++)
            {
                if (skidMarks[i].Item2 && newPath)
                {
                    gp.StartFigure();
                    skidPoints = new List<PointF>
                    {
                        new PointF((float)skidMarks[i].Item1.X, (float)skidMarks[i].Item1.Y)
                    };
                    newPath = false;
                }
                else if (skidMarks[i].Item2 && !newPath)
                {
                    skidPoints.Add(new PointF((float)skidMarks[i].Item1.X,
                        (float)skidMarks[i].Item1.Y));
                }
                else if (!skidMarks[i].Item2 && !newPath)
                {
                    if (skidPoints.Count > 0)
                        gp.AddLines(skidPoints.ToArray());
                    skidPoints = null;
                    newPath = true;
                }
            }

            if (skidPoints != null && skidPoints.Count > 0)
                gp.AddLines(skidPoints.ToArray());

            g.DrawPath(Pens.Gray, gp);

            var dir = zeroRotation.Rotate(car.Direction.AsRadians().Value).WithLength(5);
            var dirPerp = dir.RightNormal.WithLength(2);

            var p1 = car.Position + dir + dirPerp;
            var p2 = car.Position + dir - dirPerp;
            var p3 = car.Position - dir - dirPerp;
            var p4 = car.Position - dir + dirPerp;

            var points = new[] { new PointF((float)p1.X, (float)p1.Y),
                new PointF((float)p2.X, (float)p2.Y),
                new PointF((float)p3.X, (float)p3.Y),
                new PointF((float)p4.X, (float)p4.Y),
                new PointF((float)p1.X, (float)p1.Y) };

            var path = new GraphicsPath();
            path.AddLines(points);

            g.DrawPath(carPen, path);

            using Font f = new("Arial", 8.0f);
            SizeF size;

            for (int i = 0; i < 9; i++)
            {
                var angle = car.Direction + new Degrees(-60.0 + i * 15.0);
                var dist = Math.Min(seekingRange.Value, distances[i].distance);
                var vector = zeroRotation
                    .Rotate(angle.AsRadians().Value)
                    .WithLength(dist);

                string distText = $"{dist:#.#}";

                g.DrawLine(distLinePen,
                    (float)car.Position.X,
                    (float)car.Position.Y,
                    (float)(car.Position.X + vector.X),
                    (float)(car.Position.Y + vector.Y));

                var textVector = zeroRotation
                    .Rotate(angle.AsRadians().Value)
                    .WithLength(60.0);

                size = g.MeasureString(distText, f);

                DrawOutlinedText(g, distText, f, distLineTextBrush, (float)(car.Position.X + textVector.X - size.Width / 2),
                    (float)(car.Position.Y + textVector.Y - size.Height / 2));
            }

            var speedVector = zeroRotation
                .Rotate(Math.PI)
                .Rotate(car.Direction.AsRadians().Value)
                .WithLength(30);

            string speedText = $"{car.Orientation.Length:#.#}";
            size = g.MeasureString(speedText, f);

            DrawOutlinedText(g, speedText, f, distLineTextBrush, (float)(car.Position.X + speedVector.X - size.Width / 2),
                    (float)(car.Position.Y + speedVector.Y - size.Height / 2));

            output.Save($"D:\\Frame{frame}.png");
        }

        public static SimulationResult Simulate(Track track, 
            ICarFactory carFactory, 
            SimulationParams simulationParams, 
            IDecisionMaker decisionMaker)
        {
            // Initialize

            var car = carFactory.BuildCar(track.Start, track.StartOrientation);

            // Simulate

            List<(double distance, Vector2D? point)> distances = new(new (double distance, Vector2D? point)[9]);
            List<double> distanceValues = new(new double[9]);

            bool continueSimulation = true;
            int frame = 0;
            Seconds simulationTime = Seconds.Zero;

            List<(Vector2D position, bool slipping)> positions = new() { (car.Position, false) };
            bool finished = false;
            bool crashed = false;

            while (continueSimulation)
            {
                // Evaluate parameters for decision making
                double speedParam = car.NormalizedSpeed;
                double turningParam = car.NormalizedRotationRate;

                for (int i = 0; i < 9; i++)
                {
                    var angle = car.Direction + new Degrees(-60.0 + 15.0 * i);
                    Vector2D seekingVector = zeroRotation
                        .Rotate(angle.AsRadians().Value)
                        .WithLength(track.MaxDistance);

                    distances[i] = track.ObstacleDistance(new FixedVector2D(car.Position, car.Position + seekingVector));
                    distanceValues[i] = 1.0 - (distances[i].distance < (3 * track.MaxRoadWidth) ? distances[i].distance / (3 * track.MaxRoadWidth) : 1.0);
                }

                int carPosX = (int)car.Position.X;
                int carPosY = (int)car.Position.Y;

                double normalizedCourseDiff = 0.0;

                if (carPosX >= 0 && carPosX < track.GuideArray.GetLength(0) &&
                    carPosY >= 0 && carPosY < track.GuideArray.GetLength(1))
                {
                    var closestGuideIndex = Math.Min(track.Guide.Count - 2, track.GuideArray[carPosX, carPosY]);
                    var targetGuideIndex = Math.Min(track.Guide.Count - 1, closestGuideIndex + 2);

                    var targetVector = new FixedVector2D(car.Position.X, car.Position.Y, track.Guide[targetGuideIndex].X, track.Guide[targetGuideIndex].Y);
                    var targetAngle = targetVector.SpanningVector.Angle;

                    var diff = car.Direction.AsRadians().Value - targetAngle;

                    if (diff > Math.PI)
                        diff = -(2 * Math.PI - diff);

                    normalizedCourseDiff = diff / Math.PI;
                }                

                var decision = decisionMaker.Decide(speedParam,
                    turningParam,
                    normalizedCourseDiff,
                    distanceValues[0],
                    distanceValues[1],
                    distanceValues[2],
                    distanceValues[3],
                    distanceValues[4],
                    distanceValues[5],
                    distanceValues[6],
                    distanceValues[7],
                    distanceValues[8]);

                // Simulation step

                var currentPos = car.Position;

                var clampedAccelerationDecision = decision.Acceleration.Clamp(-1.0, 1.0);
                var clampedRotationDecision = decision.TurningRate.Clamp(-1.0, 1.0);

                car.ApplyMotionChange(clampedAccelerationDecision, clampedRotationDecision, simulationParams.TimeIncrement);
                var slipping = car.Move(simulationParams.TimeIncrement);

                positions.Add((car.Position, slipping));

                // Output debug

                if (simulationParams.DebugOutput)
                    DebugOutputFrame(track, car, new Pixels(3 * track.MaxRoadWidth), distances, frame, positions);

                // Progress check

                if (carPosX >= 0 && carPosX < track.GuideArray.GetLength(0) &&
                    carPosY >= 0 && carPosY < track.GuideArray.GetLength(1))
                {
                    var closestGuide = track.GuideArray[carPosX, carPosY];

                    var timeFraction = simulationTime.Value / simulationParams.MaxSimulationTime.Value;

                    timeFraction = Math.Max(0.0, timeFraction - 0.50) / 0.50;
                    var expectedGuide = track.Guide.Count * timeFraction;

                    if (closestGuide < expectedGuide)
                    {
                        continueSimulation = false;
                    }
                }
                else
                {
                    continueSimulation = false;
                    crashed = true;
                }

                // Collision check

                var step = new FixedVector2D(currentPos, car.Position);

                if (track.MoveCrossesObstacle(step))
                {
                    continueSimulation = false;
                    crashed = true;
                }

                // Finish check

                if (track.MoveCrossesFinish(step))
                {
                    continueSimulation = false;
                    finished = true;
                }

                // Timeout check

                if (simulationTime.Value >= simulationParams.MaxSimulationTime.Value)
                {
                    continueSimulation = false;                    
                }

                // Simulation step

                simulationTime += simulationParams.TimeIncrement;
                frame++;
            }

            // Evaluate results

            int? furthestPoint = null;
            int pt = track.Guide.Count - 1;
            while (pt >= 0 && furthestPoint == null)
            {
                for (int p = 0; p < positions.Count; p++)
                {
                    var distance = positions[p].position.DistanceTo(track.Guide[pt]);
                    if (distance < track.MaxRoadWidth / 2.0)
                    {
                        // Car must have travelled near this point
                        furthestPoint = pt;
                        break;
                    }
                }

                pt--;
            }

            if (furthestPoint == null)
                furthestPoint = 0;

            return new SimulationResult(positions, furthestPoint.Value, simulationTime, finished, crashed, simulationTime, decisionMaker.Color);
        }

        public static Bitmap VisualizeResults(Track track, IEnumerable<SimulationResult> simulationResults)
        {
            var result = track.Render();

            using var g = Graphics.FromImage(result);

            using var crashRunPen = new Pen(Color.FromArgb(64, 255, 0, 0));
            using var nonCrashRunPen = new Pen(Color.FromArgb(128, 40, 80, 240));
            using var crashBrush = new SolidBrush(Color.FromArgb(128, 255, 0, 0));
            using var nonCrashBrush = new SolidBrush(Color.FromArgb(128, 40, 80, 240));

            foreach (var simulationResult in simulationResults)
            {
                var path = new GraphicsPath();
                path.StartFigure();

                for (int i = 1; i < simulationResult.Positions.Count; i++)
                {
                    (Vector2D position, bool slipping) p1 = simulationResult.Positions[i - 1];
                    (Vector2D position, bool slipping) p2 = simulationResult.Positions[i];
                    path.AddLine(new PointF((float)p1.position.X, (float)p1.position.Y), new PointF((float)p2.position.X, (float)p2.position.Y));
                }

                g.DrawPath(simulationResult.Crashed ? crashRunPen : nonCrashRunPen, path);

                (Vector2D position, bool slipping) lastPos = simulationResult.Positions.Last();

                using (var brush = new SolidBrush(simulationResult.Color))
                {
                    g.FillEllipse(brush, (float)lastPos.position.X - 3.0f, (float)lastPos.position.Y - 3.0f, 6.0f, 6.0f);
                }
            }

            return result;
        }
    }
}
