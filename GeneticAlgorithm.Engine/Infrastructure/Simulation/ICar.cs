﻿using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.Simulation
{
    public interface ICar
    {
        void ApplyMotionChange(double accelerationDecision, double rotationDecision, Seconds time);
        bool Move(Seconds time);

        Vector2D Position { get; }
        Vector2D Orientation { get; }
        Vector2D Momentum { get; }
        Degrees Direction { get; }
        double NormalizedSpeed { get; }
        double NormalizedRotationRate { get; }
    }
}
