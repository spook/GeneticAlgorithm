﻿using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.Simulation
{
    public interface ICarFactory
    {
        ICar BuildCar(Vector2D start, Degrees startOrientation);
    }
}
