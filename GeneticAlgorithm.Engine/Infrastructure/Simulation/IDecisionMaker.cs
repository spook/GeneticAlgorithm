﻿using GeneticAlgorithm.Engine.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.Simulation
{
    public interface IDecisionMaker
    {
        Decision Decide(double speedParam,
            double rotationParam,
            double normalizedCourseDiff,
            double distanceMinus60,
            double distanceMinus45,
            double distanceMinus30,
            double distanceMinus15,
            double distance0,
            double distancePlus15,
            double distancePlus30,
            double distancePlus45,
            double distancePlus60);

        Color Color { get; }
    }
}
