﻿using GeneticAlgorithm.Engine.Infrastructure.QuadTree;
using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.Simulation
{
    public class Track
    {
        private readonly Vector2D areaSpan;
        private readonly IReadOnlyList<FixedVector2D> obstacles;
        private readonly FixedVector2D finish;

        private QuadTreeRoot boundaryTree;

        public Track(Vector2D areaSpan,
            List<FixedVector2D> obstacles,
            List<Vector2D> guide,
            int[,] guideArray,
            FixedVector2D finish,
            Vector2D start,
            Degrees startOrientation,
            double maxRoadWidth)
        {
            this.areaSpan = areaSpan;
            this.obstacles = obstacles;
            Guide = guide;
            GuideArray = guideArray;
            this.finish = finish;

            Start = start;
            StartOrientation = startOrientation;
            MaxRoadWidth = maxRoadWidth;

            double len = 0.0;
            for (int i = 0; i < guide.Count - 1; i++)
                len += new FixedVector2D(guide[i], guide[i + 1]).Length;

            GuideLength = new Pixels(len);

            MaxDistance = areaSpan.Length;

            boundaryTree = new QuadTreeRoot(areaSpan, obstacles);
            // boundaryTree.Visualize();
        }

        public bool MoveCrossesObstacle(FixedVector2D move) => boundaryTree.IntersectsAny(move);

        public bool MoveCrossesFinish(FixedVector2D move) => finish.IntersectsWith(move).intersects;

        public (double distance, Vector2D? point) ObstacleDistance(FixedVector2D seekVector)
        {
            var intersections = boundaryTree.FindIntersections(seekVector);

            if (!intersections.Any())
                return (double.PositiveInfinity, null);

            var distances = intersections.Select(i =>
            {
                if (i.intersectionPoint.HasValue)
                    return (seekVector.Start.DistanceTo(i.intersectionPoint.Value), i.intersectionPoint.Value);
                else
                    return (0.0, seekVector.Start);
            });

            (double dist, Vector2D point) seed = (double.PositiveInfinity, new Vector2D(0.0, 0.0));
            var minDistance = distances.Aggregate(seed, (current, x) => current.Item1 > x.Item1 ? x : current);

            return (minDistance.dist, minDistance.point);
        }

        public Bitmap Render()
        {
            Bitmap bitmap = new Bitmap((int)Math.Ceiling(areaSpan.X), (int)Math.Ceiling(areaSpan.Y), System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                using Brush backgroundBrush = new SolidBrush(Color.FromArgb(255, 255, 255, 255));
                g.FillRectangle(backgroundBrush, 0, 0, (float)areaSpan.X, (float)areaSpan.Y);

                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                using var obstaclePen = new Pen(Color.FromArgb(255, 0, 0, 0), 2.0f);

                // Obstacles
                foreach (var obstacle in obstacles)
                    g.DrawLine(obstaclePen, (float)obstacle.Start.X, (float)obstacle.Start.Y, (float)obstacle.End.X, (float)obstacle.End.Y);

                // Finish

                using var finishPen = new Pen(Color.FromArgb(255, 255, 0, 0), 2.0f);

                g.DrawLine(finishPen, (float)finish.Start.X, (float)finish.Start.Y, (float)finish.End.X, (float)finish.End.Y);

                // Guides

                using var guideBrush = new SolidBrush(Color.FromArgb(128, 0, 255, 64));

                foreach (var point in Guide)
                    g.FillEllipse(guideBrush, (float)point.X - 2, (float)point.Y - 2, 4.0f, 4.0f);

                // Start
                Vector2D startVector = new Vector2D(0.0, 10.0).Rotate(StartOrientation.AsRadians().Value);

                using Brush startBrush = new SolidBrush(Color.FromArgb(128, 0, 64, 255));
                g.FillEllipse(startBrush, (float)Start.X - 5, (float)Start.Y - 5, 10.0f, 10.0f);

                using Pen startPen = new Pen(Color.FromArgb(128, 0, 64, 255), 3);
                g.DrawLine(startPen, (float)Start.X, (float)Start.Y, (float)(Start.X + startVector.X), (float)(Start.Y + startVector.Y));
            }

            return bitmap;
        }

        public Vector2D Start { get; }
        public Degrees StartOrientation { get; }
        public double MaxRoadWidth { get; }

        public IReadOnlyList<Vector2D> Guide { get; }
        public Pixels GuideLength { get; }
        public int[,] GuideArray { get; }

        public double MaxDistance { get; }
    }
}
