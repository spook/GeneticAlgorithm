﻿using GeneticAlgorithm.Engine.Infrastructure.QuadTree;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.Simulation
{
    public class Boundary : IQuadTreeItem
    {
        public Boundary(FixedVector2D vector)
        {
            this.Vector = vector;
        }


        public bool IntersectsRectangle(Rectangle2D rectangle) => Vector.IntersectsWith(rectangle);

        public bool IntersectsSegment(FixedVector2D segment) => Vector.IntersectsWith(segment).intersects;

        public FixedVector2D Vector { get; }
    }
}
