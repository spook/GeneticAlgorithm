﻿using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.QuadTree
{
    public interface IQuadTreeItem
    {
        bool IntersectsRectangle(Rectangle2D rectangle);
        bool IntersectsSegment(FixedVector2D segment);

        FixedVector2D Vector { get; }
    }
}
