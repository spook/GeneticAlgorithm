﻿using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.QuadTree
{
    public class QuadTreeRoot : QuadTreeNode
    {
        public QuadTreeRoot(Vector2D areaSpan, IEnumerable<FixedVector2D> items, int maxDepth = 4, int minItems = 10)
            : base(new Rectangle2D(new Vector2D(0.0, 0.0), areaSpan), items, maxDepth, minItems)
        {
            
        }

        public List<(FixedVector2D vector, Vector2D? intersectionPoint)> FindIntersections(FixedVector2D segment)
        {
            List<(FixedVector2D vector, Vector2D? intersectionPoint)> intersections = new();
            FindIntersectionsRecursive(segment, intersections);
            return intersections.ToList();
        }

        public bool IntersectsAny(FixedVector2D segment)
        {
            return IntersectsAnyRecursive(segment);
        }

        public void Visualize()
        {
            Bitmap bitmap = new Bitmap((int)Rectangle.BottomRight.X, (int)Rectangle.BottomRight.Y, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using Graphics g = Graphics.FromImage(bitmap);
            g.FillRectangle(Brushes.White, 0, 0, (int)Rectangle.BottomRight.X, (int)Rectangle.BottomRight.Y);

            this.Visualize(bitmap, Rectangle);
            bitmap.Save("D:\\tree.png");
        }
    }
}
