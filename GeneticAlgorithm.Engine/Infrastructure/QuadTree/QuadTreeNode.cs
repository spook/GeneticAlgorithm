﻿using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.QuadTree
{
    public class QuadTreeNode : BaseQuadTreeNode
    {
        protected readonly IReadOnlyList<BaseQuadTreeNode> subItems;

        internal override void FindIntersectionsRecursive(FixedVector2D segment, List<(FixedVector2D vector, Vector2D? intersectionPoint)> intersections)
        {
            foreach (var subItem in subItems)
            {
                if (subItem.Rectangle.IntersectsWith(segment))
                {
                    subItem.FindIntersectionsRecursive(segment, intersections);
                }
            }
        }

        internal override bool IntersectsAnyRecursive(FixedVector2D segment)
        {
            foreach (var subItem in subItems)
            {
                if (subItem.Rectangle.IntersectsWith(segment))
                    if (subItem.IntersectsAnyRecursive(segment))
                        return true;
            }

            return false;
        }

        public QuadTreeNode(Rectangle2D rectangle, IEnumerable<FixedVector2D> items, int maxDepth, int minItems)
            : base(rectangle)
        {
            var subItems = new List<BaseQuadTreeNode>();

            double[] xCoords = new[] { rectangle.TopLeft.X, rectangle.TopLeft.X + (rectangle.BottomRight.X - rectangle.TopLeft.X) / 2, rectangle.BottomRight.X };
            double[] yCoords = new[] { rectangle.TopLeft.Y, rectangle.TopLeft.Y + (rectangle.BottomRight.Y - rectangle.TopLeft.Y) / 2, rectangle.BottomRight.Y };

            List<Rectangle2D> subRectangles = new()
            {
                new Rectangle2D(xCoords[0], yCoords[0], xCoords[1], yCoords[1]),
                new Rectangle2D(xCoords[1], yCoords[0], xCoords[2], yCoords[1]),
                new Rectangle2D(xCoords[0], yCoords[1], xCoords[1], yCoords[2]),
                new Rectangle2D(xCoords[1], yCoords[1], xCoords[2], yCoords[2])
            };

            foreach (var subRectangle in subRectangles)
            {
                List<FixedVector2D> matchingItems = items.Where(i => i.IntersectsWith(subRectangle)).ToList();

                if (maxDepth == 0 || matchingItems.Count <= minItems)
                {
                    var leaf = new QuadTreeLeaf(subRectangle, matchingItems);
                    subItems.Add(leaf);
                }
                else
                {
                    var node = new QuadTreeNode(subRectangle, matchingItems, maxDepth - 1, minItems);
                    subItems.Add(node);
                }
            }

            this.subItems = subItems;
        }

        internal override void Visualize(Bitmap bitmap, Rectangle2D area)
        {
            for (int i = 0; i < subItems.Count; i++)
                subItems[i].Visualize(bitmap, area);
        }
    }
}
