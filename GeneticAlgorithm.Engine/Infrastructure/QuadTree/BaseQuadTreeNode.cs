﻿using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.QuadTree
{
    public abstract class BaseQuadTreeNode
    {
        protected BaseQuadTreeNode(Rectangle2D rectangle)
        {
            Rectangle = rectangle;
        }

        internal abstract void FindIntersectionsRecursive(FixedVector2D segment, List<(FixedVector2D vector, Vector2D? intersectionPoint)> intersections);

        internal abstract bool IntersectsAnyRecursive(FixedVector2D segment);

        public Rectangle2D Rectangle { get; }

        internal abstract void Visualize(Bitmap bitmap, Rectangle2D area);
    }
}
