﻿using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.QuadTree
{
    public class QuadTreeLeaf : BaseQuadTreeNode
    {
        private List<FixedVector2D> items;

        public QuadTreeLeaf(Rectangle2D rectangle, IEnumerable<FixedVector2D> items)
            : base(rectangle)
        {
            this.items = items.ToList();
        }

        internal override void FindIntersectionsRecursive(FixedVector2D segment, List<(FixedVector2D vector, Vector2D? intersectionPoint)> intersections)
        {
            foreach (var item in items)
            {
                (bool intersects, Vector2D? intersectionPoint) = item.IntersectsWith(segment);
                if (intersects)
                    intersections.Add((item, intersectionPoint));
            }
        }

        internal override bool IntersectsAnyRecursive(FixedVector2D segment)
        {
            return items.Any(item => item.IntersectsWith(segment).intersects);
        }

        internal override void Visualize(Bitmap bitmap, Rectangle2D area)
        {            
            using Graphics g = Graphics.FromImage(bitmap);

            using var framePen = new Pen(Color.FromArgb(128, 0, 0, 0));
            g.DrawRectangle(framePen, (float)this.Rectangle.TopLeft.X,
                (float)this.Rectangle.TopLeft.Y,
                (float)(this.Rectangle.BottomRight.X - this.Rectangle.TopLeft.X),
                (float)(this.Rectangle.BottomRight.Y - this.Rectangle.TopLeft.Y));

            using var itemPen = new Pen(Color.FromArgb(255, 255, 0, 0));
            foreach (var item in items)
            {
                g.DrawLine(itemPen, (float)item.Start.X, (float)item.Start.Y,
                    (float)item.End.X, (float)item.End.Y);
            }
        }
    }
}
