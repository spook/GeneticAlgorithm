﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.NeuralNetwork
{
    public class Network
    {
        internal Network(int inputSize, IReadOnlyList<Layer> layers)
        {
            if (layers.Count < 1)
                throw new ArgumentException("Network is too small!");
            InputSize = inputSize;
            this.Layers = layers;
        }

        public List<double> Evaluate(IReadOnlyList<double> input)
        {
            for (int n = 0; n < Layers[0].Neurons.Count; n++)
                Layers[0].Neurons[n].Eval(input);

            for (int l = 1; l < Layers.Count; l++)
                for (int n = 0; n < Layers[l].Neurons.Count; n++)
                    Layers[l].Neurons[n].Eval(Layers[l - 1]);

            return Layers[Layers.Count - 1].Neurons
                .Select(n => n.Value)
                .ToList();
        }

        public Network Clone()
        {
            List<Layer> layersClone = new List<Layer>();
            for (int i = 0; i < Layers.Count; i++)
                layersClone.Add(Layers[i].Clone());

            return new Network(InputSize, layersClone);
        }

        public int InputSize { get; }
        public IReadOnlyList<Layer> Layers { get; }
    }
}
