﻿using GeneticAlgorithm.Engine.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.NeuralNetwork
{
    public class Layer : IReadOnlyList<double>
    {
        private static readonly Random random = new();

        public int Count => Neurons.Count;

        public IEnumerator<double> GetEnumerator()
        {
            return Neurons.Select(n => n.Value).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Neurons.Select(n => n.Value).GetEnumerator();
        }

        private Layer(IReadOnlyList<Neuron> neurons)
        {
            this.Neurons = neurons;
        }

        public Layer(int previousLayerLength, int length, ActivationFunction activationFunction, double randomRange)
        {
            var neurons = new List<Neuron>();
            for (int i = 0; i < length; i++)
                neurons.Add(new Neuron(previousLayerLength, randomRange, activationFunction, random));

            Neurons = neurons;
        }

        public Layer Clone()
        {
            var neurons = new List<Neuron>();
            for (int i = 0; i < Neurons.Count; i++)
                neurons.Add(Neurons[i].Clone());

            return new Layer(neurons);
        }

        public double this[int index] => Neurons[index].Value;

        public IReadOnlyList<Neuron> Neurons { get; }

        public override string ToString()
        {
            return String.Join("\r\n", Neurons.Select(n => n.ToString()));
        }
    }
}
