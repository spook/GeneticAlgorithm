﻿using GeneticAlgorithm.Engine.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.NeuralNetwork
{
    public class Neuron
    {
        private List<(double weight, double bias)> factors;
        private readonly ActivationFunction activationFunction;

        private static double GenerateRandom(Random random, double randomRange)
        {
            return random.NextDouble() * 2 * randomRange - randomRange;
        }

        private Neuron(List<(double weight, double bias)> factors, ActivationFunction activationFunction)
        {
            this.factors = factors;
            this.activationFunction = activationFunction;
        }

        public Neuron(int inputCount, double randomRange, ActivationFunction activationFunction, Random random)
        {
            factors = new();

            for (int i = 0; i < inputCount; i++)
                factors.Add((GenerateRandom(random, randomRange), GenerateRandom(random, randomRange)));

            this.activationFunction = activationFunction;
        }

        public void Eval(IReadOnlyList<double> input)
        {
            if (input.Count != factors.Count)
                throw new ArgumentOutOfRangeException(nameof(input));

            var value = input
                .Zip(factors, (n, f) => n * f.weight + f.bias)
                .Sum();

            Value = activationFunction switch
            {
                ActivationFunction.Sigmoid => 2 / (1 + Math.Exp(-value)) - 1,
                ActivationFunction.Tanh => Math.Tanh(value),
                ActivationFunction.ReLu => value < 0 ? 0.0 : value,
                _ => throw new InvalidOperationException("Unsupported activation function!")
            };
        }

        public void Replace(int neuronIndex, (double weight, double bias) newFactor)
        {
            factors[neuronIndex] = newFactor;
        }

        public Neuron Clone()
        {
            var factorsClone = new List<(double weight, double bias)>();

            for (int i = 0; i < factors.Count; i++)
                factorsClone.Add(factors[i]);

            return new Neuron(factorsClone, activationFunction);
        }

        public IReadOnlyList<(double weight, double bias)> Factors => factors;
        public double Value { get; private set; } = 0.0;

        public override string ToString()
        {
            return string.Join(", ", factors.Select(f => $"{f.weight:#.##} + {f.bias:#.##}"));
        }
    }
}
