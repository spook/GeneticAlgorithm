﻿using GeneticAlgorithm.Engine.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Infrastructure.NeuralNetwork
{
    public class NetworkBuilder
    {
        int? previousLength = null;
        int? inputSize = null;
        private List<Layer> layers = new();

        public NetworkBuilder AddInputLayer(int length)
        {
            if (layers == null)
                throw new InvalidOperationException("Cannot use network builder after building a network!");

            if (inputSize != null)
                throw new InvalidOperationException("Input layer is already added");

            previousLength = length;
            inputSize = length;
            return this;
        }

        public NetworkBuilder AddLayer(int length, ActivationFunction activationFunction, double randomRange = 1.0f)
        {
            if (layers == null)
                throw new InvalidOperationException("Cannot use network builder after building a network!");

            if (inputSize == null)
                throw new InvalidOperationException("Input layer is missing!");

            var layer = new Layer(previousLength.Value, length, activationFunction, randomRange);
            layers.Add(layer);
            previousLength = length;

            return this;
        }

        public Network Build()
        {
            if (layers == null)
                throw new InvalidOperationException("Cannot use network builder after building a network!");

            if (layers.Count < 1)
                throw new InvalidOperationException("Not enough layers! (Requires at least 2)");

            var result = new Network(inputSize.Value, layers);
            layers = null;
            inputSize = null;
            previousLength = null;
            return result;
        }
    }
}
