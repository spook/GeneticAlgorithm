﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Utils
{
    public static class DoubleExtensions
    {
        public static double Wrap(this double value, double range)
        {
            if (range <= 0)
                throw new ArgumentOutOfRangeException(nameof(range));

            while (value < 0.0)
                value += range;
            while (value > range)
                value -= range;

            return value;
        }

        public static double Clamp(this double value, double min, double max)
        {
            if (min > max)
                throw new ArgumentOutOfRangeException(nameof(min));

            return Math.Max(min, Math.Min(max, value));
        }
    }
}
