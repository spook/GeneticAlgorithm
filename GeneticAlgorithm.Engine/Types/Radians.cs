﻿using GeneticAlgorithm.Engine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct Radians
    {
        public Radians(double value)
        {
            Value = value;
        }

        public Degrees AsDegrees() => new(Value / (2 * Math.PI) * 360.0f);

        public static Radians operator + (Radians first, Radians second)
        {
            return new((first.Value + second.Value).Wrap(2 * Math.PI));
        }

        public static Radians operator -(Radians first, Radians second)
        {
            return new((first.Value - second.Value).Wrap(2 * Math.PI));
        }

        public double Value { get; }

        public static Radians Zero { get; } = new(0.0f);
    }
}
