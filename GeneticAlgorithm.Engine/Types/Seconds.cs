﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct Seconds
    {
        public Seconds(double value)
        {
            Value = value;
        }

        public static Seconds operator + (Seconds first, Seconds second)
        {
            return new(first.Value + second.Value);
        }

        public static Seconds operator - (Seconds first, Seconds second)
        {
            return new(first.Value - second.Value);
        }

        public static SecondsSquared operator * (Seconds first, Seconds second)
        {
            return new(first.Value * second.Value);
        }

        public double Value { get; }

        public static Seconds Zero { get; } = new Seconds(0.0f);
        
        public static Seconds One { get; } = new Seconds(1.0f);
    }
}
