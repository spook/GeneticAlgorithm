﻿using GeneticAlgorithm.Engine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct Degrees
    {
        public Degrees(double value)
        {
            Value = value;
        }

        public Degrees ApplySpeed(DegreesPerSecond rotationalSpeed, Seconds time)
        {
            return new Degrees(Value + rotationalSpeed.Value * time.Value);
        }

        public Radians AsRadians() => new((Value / 360.0f * (2 * Math.PI)).Wrap(2 * Math.PI));

        public static Degrees operator + (Degrees first, Degrees second)
        {
            return new Degrees((first.Value + second.Value).Wrap(360.0f));
        }

        public static Degrees operator - (Degrees first, Degrees second)
        {
            return new Degrees((first.Value - second.Value).Wrap(360.0f));
        }

        public double Value { get; }

        public static Degrees Zero { get; } = new(0.0f);
    }
}
