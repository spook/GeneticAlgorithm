﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct DegreesPerSecondSquared
    {
        public DegreesPerSecondSquared(double value)
        {
            Value = value;
        }

        public DegreesPerSecondSquared Clamp(DegreesPerSecondSquared min, DegreesPerSecondSquared max)
        {
            if (min.Value > max.Value)
                throw new ArgumentOutOfRangeException(nameof(min));

            return new(Math.Min(max.Value, Math.Max(min.Value, Value)));
        }

        public static DegreesPerSecond operator *(DegreesPerSecondSquared acceleration, Seconds time)
        {
            return new(acceleration.Value * time.Value);
        }

        public static DegreesPerSecond operator *(Seconds time, DegreesPerSecondSquared acceleration)
        {
            return new(acceleration.Value * time.Value);
        }

        public static DegreesPerSecondSquared operator *(DegreesPerSecondSquared first, double second)
        {
            return new(first.Value * second);
        }

        public static DegreesPerSecondSquared operator *(double first, DegreesPerSecondSquared second)
        {
            return new(second.Value * first);
        }

        public static DegreesPerSecondSquared operator - (DegreesPerSecondSquared value)
        {
            return new(-value.Value);
        }

        public double Value { get; }

        public static DegreesPerSecondSquared Zero { get; } = new(0.0f);
    }
}
