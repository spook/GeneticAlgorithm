﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct DegreesPerSecond
    {
        public DegreesPerSecond(double value)
        {
            Value = value;
        }

        public DegreesPerSecond Clamp(DegreesPerSecond min, DegreesPerSecond max)
        {
            if (min.Value >= max.Value)
                throw new ArgumentOutOfRangeException(nameof(min));

            return new(Math.Max(min.Value, Math.Min(max.Value, Value)));
        }

        public static Degrees operator * (DegreesPerSecond rotationalSpeed, Seconds time)
        {
            return new(rotationalSpeed.Value * time.Value);
        }

        public static Degrees operator * (Seconds time, DegreesPerSecond rotationalSpeed)
        {
            return new(rotationalSpeed.Value * time.Value);
        }

        public static DegreesPerSecond operator + (DegreesPerSecond first, DegreesPerSecond second)
        {
            return new(first.Value + second.Value);
        }

        public static DegreesPerSecond operator -(DegreesPerSecond first, DegreesPerSecond second)
        {
            return new(first.Value - second.Value);
        }

        public static DegreesPerSecond operator -(DegreesPerSecond value)
        {
            return new(-value.Value);
        }

        public static double operator / (DegreesPerSecond first, DegreesPerSecond second)
        {
            return first.Value / second.Value;
        }

        public double Value { get; }

        public static DegreesPerSecond Zero { get; } = new DegreesPerSecond(0.0);
    }
}
