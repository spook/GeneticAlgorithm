﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct PixelsPerSecond
    {
        public PixelsPerSecond(double value)
        {
            Value = value;
        }

        public PixelsPerSecond Clamp(PixelsPerSecond min, PixelsPerSecond max)
        {
            if (min.Value >= max.Value)
                throw new ArgumentOutOfRangeException(nameof(min));

            return new(Math.Max(min.Value, Math.Min(max.Value, Value)));
        }

        public static PixelsPerSecond operator + (PixelsPerSecond first, PixelsPerSecond second)
        {
            return new(first.Value + second.Value);
        }

        public static Pixels operator * (PixelsPerSecond speed, Seconds time)
        {
            return new(speed.Value * time.Value);
        }

        public static Pixels operator * (Seconds time, PixelsPerSecond speed)
        {
            return new(speed.Value * time.Value);
        }

        public static double operator / (PixelsPerSecond first, PixelsPerSecond second)
        {
            return first.Value / second.Value;
        }

        public static Seconds operator / (PixelsPerSecond first, PixelsPerSecondSquared second)
        {
            return new Seconds(first.Value / second.Value);
        }

        public static PixelsPerSecond operator - (PixelsPerSecond first)
        {
            return new(-first.Value);
        }

        public double Value { get; }

        public static PixelsPerSecond Zero { get; } = new(0.0f);
    }
}
