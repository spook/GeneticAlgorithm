﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct Pixels
    {
        public Pixels(double value)
        {
            Value = value;
        }

        public static Pixels operator + (Pixels first, Pixels second)
        {
            return new(first.Value + second.Value);
        }

        public static Pixels operator -(Pixels first, Pixels second)
        {
            return new(first.Value - second.Value);
        }

        public static Pixels operator * (Pixels first, double second)
        {
            return new(first.Value * second);
        }

        public static Pixels operator * (double first, Pixels second)
        {
            return new(second.Value * first);
        }

        public static Pixels operator / (Pixels first, double second)
        {
            return new(first.Value / second);
        }

        public double Value { get; }
    }
}
