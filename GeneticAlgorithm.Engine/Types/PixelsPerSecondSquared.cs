﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Types
{
    public record struct PixelsPerSecondSquared
    {
        public PixelsPerSecondSquared(double value)
        {
            Value = value;
        }

        public PixelsPerSecondSquared Clamp(PixelsPerSecondSquared min, PixelsPerSecondSquared max)
        {
            if (min.Value > max.Value)
                throw new ArgumentOutOfRangeException(nameof(min));

            return new(Math.Min(max.Value, Math.Max(min.Value, Value)));
        }

        public static PixelsPerSecond operator * (PixelsPerSecondSquared acceleration, Seconds time)
        {
            return new(acceleration.Value * time.Value);
        }

        public static PixelsPerSecond operator * (Seconds time, PixelsPerSecondSquared acceleration)
        {
            return new(acceleration.Value * time.Value);
        }
        
        public static Pixels operator * (PixelsPerSecondSquared first, SecondsSquared second)
        {
            return new(first.Value * second.Value);
        }

        public static Pixels operator *(SecondsSquared first, PixelsPerSecondSquared second)
        {
            return new(first.Value * second.Value);
        }

        public static PixelsPerSecondSquared operator * (PixelsPerSecondSquared first, double second)
        {
            return new(first.Value * second);
        }

        public static PixelsPerSecondSquared operator *(double first, PixelsPerSecondSquared second)
        {
            return new(second.Value * first);
        }

        public static PixelsPerSecondSquared operator - (PixelsPerSecondSquared value)
        {
            return new(-value.Value);
        }

        public double Value { get; }

        public static PixelsPerSecondSquared Zero { get; } = new(0.0f);
    }
}
