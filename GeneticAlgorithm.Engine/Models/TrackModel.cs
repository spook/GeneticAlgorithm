﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GeneticAlgorithm.Engine.Models
{
    [XmlRoot("Track")]
    public class TrackModel
    {
        [XmlElement("Area")]
        public Size Area { get; set; }
        [XmlElement("Obstacles")]
        public string Obstacles { get; set; }
        [XmlElement("Finish")]
        public Line Finish { get; set; }
        [XmlElement("Start")]
        public DirectionalPoint Start { get; set; }
        [XmlElement("Guide")]
        public string Guide { get; set; }
        [XmlElement("MaxRoadWidth")]
        public double MaxRoadWidth { get; set; }
    }
}
