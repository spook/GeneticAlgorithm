﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GeneticAlgorithm.Engine.Models
{
    [XmlRoot("DirectionalPoint")]
    public class DirectionalPoint
    {
        [XmlAttribute]
        public double X { get; set; }
        [XmlAttribute]
        public double Y { get; set; }
        [XmlAttribute]
        public double Orientation { get; set; }
    }
}
