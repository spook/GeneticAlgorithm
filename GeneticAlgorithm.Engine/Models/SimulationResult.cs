﻿using GeneticAlgorithm.Engine.Types;
using Spooksoft.Geometry.TwoDimensional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Models
{
    public class SimulationResult
    {
        public SimulationResult(IReadOnlyList<(Vector2D, bool)> positions,
            int furthestPoint,
            Seconds driveTime,
            bool finished,
            bool crashed,
            Seconds simulationTime,
            Color color) 
        {
            Positions = positions;
            FurthestPoint = furthestPoint;
            DriveTime = driveTime;
            Finished = finished;
            Crashed = crashed;
            SimulationTime = simulationTime;
            Color = color;
        }

        public IReadOnlyList<(Vector2D, bool)> Positions { get; }
        public int FurthestPoint { get; }
        public Pixels EstimatedLengthTraveled { get; }
        public Seconds DriveTime { get; }
        public bool Finished { get; }
        public bool Crashed { get; }
        public Seconds SimulationTime { get; }
        public Color Color { get; }
    }
}
