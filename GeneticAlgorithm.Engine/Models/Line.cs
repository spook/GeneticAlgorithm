﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GeneticAlgorithm.Engine.Models
{
    [XmlRoot("Line")]
    public class Line
    {
        [XmlAttribute]
        public double X1 { get; set; }
        [XmlAttribute]
        public double Y1 { get; set; }
        [XmlAttribute]
        public double X2 { get; set; }
        [XmlAttribute]
        public double Y2 { get; set; }
    }
}
