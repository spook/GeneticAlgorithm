﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Models
{
    public record struct Decision(double Acceleration, double TurningRate)
    {

    }
}
