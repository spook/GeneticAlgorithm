﻿using GeneticAlgorithm.Engine.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Engine.Models
{
    public record class SimulationParams
    {
        public SimulationParams(bool debugOutput, Seconds timeIncrement, Seconds maxSimulationTime)
        {
            DebugOutput = debugOutput;
            TimeIncrement = timeIncrement;
            MaxSimulationTime = maxSimulationTime;
        }

        public bool DebugOutput { get; init; }
        public Seconds TimeIncrement { get; init; }
        public Seconds MaxSimulationTime { get; init; }
    }
}
